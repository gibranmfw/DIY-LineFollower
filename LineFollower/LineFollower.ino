// Motor Kiri
const int motorKiri = 10;
const int enKiri = 9;

// Motor Kanan
const int motorKanan = 6;
const int enKanan = 4;

// other variable
const int batas = 20;

int state1 = 0;
int state2 = 0;
int currentState = 1;
int counter = 1;

int s1 = 0;
int s2 = 0;
int s3 = 0;
int s4 = 0;
int s5 = 0;
int s6 = 0;

void setup() {
  // set motor pin
  pinMode(motorKiri, OUTPUT);
  pinMode(motorKanan, OUTPUT);
  pinMode(enKiri, OUTPUT);
  pinMode(enKanan , OUTPUT);
  
  // for serial communication
  Serial.begin(9600);
  
}

void loop() {
  s1 = analogRead(A0);
  s2 = analogRead(A1);
  s3 = analogRead(A2);
  s4 = analogRead(A3);
  s5 = analogRead(A4);
  s6 = analogRead(A5);
  
  Serial.println(s1);
  Serial.println(s2);
  Serial.println(s3);
  Serial.println(s4);
  Serial.println(s5);
  Serial.println(s6);

  int result = checkInput();
  updateState(result); 
  followLine(result);
  checkState();

  Serial.println("------ ");

}

void forward() {
  analogWrite(motorKiri, 120);
  digitalWrite(enKiri, HIGH);

  digitalWrite(enKanan, HIGH);
  analogWrite(motorKanan, 135);
}

void forward75() {
  analogWrite(motorKiri, 120);
  digitalWrite(enKiri, HIGH);

  digitalWrite(enKanan, HIGH);
  analogWrite(motorKanan, 115);
  
  delay(300);
}

void turnRight() {
  analogWrite(motorKiri, 120);
  digitalWrite(enKiri, HIGH);

  digitalWrite(enKanan, HIGH);
  analogWrite(motorKanan, 255);
  
  delay(70);
}

void turnLeft() {
  analogWrite(motorKiri, 0);
  digitalWrite(enKiri, HIGH);

  digitalWrite(enKanan, HIGH);
  analogWrite(motorKanan, 145);
  
  delay(70);
}

void turnRight90() {
  
  analogWrite(motorKiri, 120);
  digitalWrite(enKiri, HIGH);

  digitalWrite(enKanan, HIGH);
  analogWrite(motorKanan, 255);
  
  delay(900);
  
}

void turnLeft90() {
  analogWrite(motorKiri, 0);
  digitalWrite(enKiri, HIGH);

  digitalWrite(enKanan, HIGH);
  analogWrite(motorKanan, 170);
  
  delay(900);
}

void muterKanan() {
   analogWrite(motorKiri, 120);
  digitalWrite(enKiri, HIGH);

  digitalWrite(enKanan, HIGH);
  analogWrite(motorKanan, 255);
  
  delay(1500);
}

void muterKiri() {
  analogWrite(motorKiri, 0);
  digitalWrite(enKiri, HIGH);

  digitalWrite(enKanan, HIGH);
  analogWrite(motorKanan, 125);
  
  delay(1500);
}

void stops() {
  digitalWrite(enKanan, LOW);
  digitalWrite(enKiri, LOW);
}

int checkInput() {
    int input = B000000;
    if(s6 < batas) {
      input += B000000;
    }
    else {
      input += B000001;
    }
    if(s5 < batas) {
      input += B000000;
    }
    else {
      input += B000010;
    }
    if(s4 < batas) {
      input += B000000;
    }
    else {
      input += B000100;
    }
    if(s3 < batas) {
      input += B000000;
    }
    else {
      input += B001000;
    }
    if(s2 < batas) {
      input += B000000;
    }
    else {
      input += B010000;
    }
    if(s1 < batas) {
      input += B000000;
    }
    else {
      input += B100000;
    }
    return input;
  }

void followLine(int input) {
    if(input == B110011 ||  input == B000000 || input == B100001 || input == B000001 || input == B100000 || input == B1100001) {
      Serial.println("maju");
      forward();
    }
    else if (input == B110111 || input == B101111 || input == B100111 || input == B001111 || input == B100011) {
      Serial.println("belok kiri");
      turnLeft();
    }
    else if(counter % 2 == 1 && (input == B101101 || input == B001100)) {
      Serial.println("belok kiri");
      turnLeft();
      counter++; 
    }
    else if(counter % 2 == 0 && (input == B101101 || input == B001100)) {
      Serial.println("belok kanan");
      turnRight();
      counter++;
    }

    else if(input == B011111 || input == B001111 || input == B000111 ||  input == B000011) {
      Serial.println("belok kiri 90 derajat");
      turnLeft90();
    }

    else if(input == B111011 || input ==  B111101 || input == B111001 || input == B111100) {
      Serial.println("belok kanan");
      turnRight();
    }

    else if(input == B111110 || input == B111100 || input == B111000 || input == B110000) {
      Serial.println("belok kanan 90 derajat");
      turnRight90();
    }

    else if(input == B111111) {
      if(previousState() != B111111) {
        Serial.println("Menjalani State Sebelumnya");
        forward75();
      }
      else {
        Serial.println("muter 180 derajat");  
        muterKanan();
        forward();
      }
    }
  }

void checkState() {
    currentState++;
    if(currentState > 2) {
      currentState = 1;
    }
}

void updateState(int value) {
  if(currentState == 1) {
    state1 = value;
    }
  else {
    state2 = value;
    }
}

int previousState() {
  if(currentState == 1) {
    return state2;
    }
  else {
    return state1;
    }
}
